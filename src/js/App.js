
import React from 'react';
import Home from './Home';
import Cat from './Cat';
import { BrowserRouter as Router, Route } from 'react-router-dom';


function App() {
  return (
    <div className="App">
     <Router>
        <Route path="/" exact component={Home} />
        <Route path="/:id" exact component={Cat} />
      </Router>
    </div>
  );
}

export default App;
