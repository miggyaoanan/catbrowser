
import React, { Component } from 'react';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';

import { findIndex, isUndefined } from 'lodash';
import { Link } from 'react-router-dom';
import '../css/Home.scss';

//this is for the URL OBJ 
export function fromQuery(url) {
  const string = url.indexOf('?');
  if (string < 0) {
    return {};
  }
  const object = {},
        hash = url.indexOf('#', string),
        query = url.substring(string + 1, (hash < 0) ? undefined : hash);
  query.split('&').forEach((pair) => {
    const [name, value] = pair.split('=');
    object[name] = isUndefined(value) ? true : decodeURIComponent(value);
  });
  return object;
}

//HOME COMPONENT
export default class Home extends Component {

    constructor(...args) {
        super(...args);
        this.state = {
          loading: false,
          cats: [],
          breed: '',
          breeds: [],
          catsRemaining: false,
          page: 1,
          ready: false
        }
      }

    // This is to load the list breed fucntion from the API
    componentDidMount() {
        axios.get('//api.thecatapi.com/v1/breeds').then(({ data }) => {
          this.setState({
            breeds: data,
            ready: true
          });
          const query = fromQuery(this.props.location.search);
          if (query.breed) {
            this.select(query.breed);
          }
        });
      }

 // this is to set the select 
  select(breed) {
    this.setState({
      breed,
      cats: []
    });
    if (breed) {
      this.load(1, breed);
    }
  }

  // this is to load the page - 

   load(page, breed = this.state.breed) {
    axios(`//api.thecatapi.com/v1/images/search?page=${page}&limit=10&breed_id=${breed}`).then(({ data }) => {
      const cats = [];
      data.forEach((cat) => {
        if (findIndex(this.state.cats, ({ id }) => (id === cat.id)) < 0) {
          cats.push(cat);
        }
      })
      this.setState({
        loading: false,
        cats: [
          ...this.state.cats,
          ...cats
        ],
        catsRemaining: (cats.length === 0)
      });
    });
    this.setState({
      loading: true,
      page
    });
  }

  

  render() {
    const { breed, breeds, loading, cats, catsRemaining, page, ready } = this.state;
    return (
      <div className="Home">
        <Container>
          <h1>Cat Browser</h1>
          <Row style={{ padding: '10px 0' }}>
            <Col md={3} sm={6} xs={12}>
              <Form.Group controlId="breed">
                <Form.Label>Breed</Form.Label>
                <Form.Control disabled={!ready || loading} as="select" value={breed} onChange={(e) => { this.select(e.target.value); }}>
                  <option value="">Select breed</option>
                  {breeds.map(({ id, name }) => (
                    <option key={id} value={id}>{name}</option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            {(!cats.length ?
              <Col xs={12} style={{ marginBottom: '20px' }}>No cats available</Col> :
              cats.map(({ id, url }, i) => (
                <Col md={3} sm={6} xs={12} key={i}>
                  <Card>
                    <Card.Img variant="top" src={url} />
                    <Card.Body>
                      <Link className="btn btn-primary btn-block" to={'/' + id}>View details</Link>
                    </Card.Body>
                  </Card>
                </Col>
              ))
            )}
          </Row>
          {(catsRemaining ? '' :
            <Row>
              <Col md={3} sm={6} xs={12}>
                <Button variant="success" disabled={!breed || loading} type="button" onClick={() => { this.load(page + 1) }}>
                  {loading ? 'Loading cats...' : 'Load more'}
                </Button>
              </Col>
            </Row>
          )}
        </Container>
      </div>
    );
  }

 

  

  
}
